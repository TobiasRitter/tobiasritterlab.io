# tobiasritter.gitlab.io

This repository contains the CI/CD pipeline to automatically deploy my [personal website](https://tobiasritter.gitlab.io). The code behind it can be found [here](https://gitlab.com/TobiasRitter/personal_website).
